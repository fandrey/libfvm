CC = gcc
CFLAGS += $(shell pkg-config --cflags libserialport)
LIBS = $(shell pkg-config --libs libserialport)
LIBFVM = libfvm.so

all: libfvm.so

$(LIBFVM): fvm.c fvm.h
	$(CC) -Wall -O2 -fPIC -shared -o $@ $^ $(CFLAGS) $(LIBS)
