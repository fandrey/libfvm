;;; Guile Scheme bindings for libfvm
;;;
;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of libfvm.
;;;
;;; libfvm is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; libfvm is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with libfvm.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (fvm)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:export (start-enroll-fv
            call-with-open-fvm
            call-with-enroll-process
            call-with-identify-fv-result
            cancel-enroll-fv
            get-enroll-fv-count))

(define %libfvm "libfvm")

(define fvm-func
  (let ((lib (dynamic-link %libfvm)))
    (lambda (return-type function-name arg-types)
      (pointer->procedure return-type
                          (dynamic-func function-name lib)
                          arg-types))))

(define-syntax-rule (define-foreign name return-type func-name arg-types)
  (define name
    (fvm-func return-type func-name arg-types)))

(define (fvm-error num)
  (assv-ref '((#x1100 . success)
              (#x0100 . fail)
              (#xb000 . store-full)
              (#x1900 . duplication-id)
              (#x1b00 . bad-quality)
              (#xb100 . timeout)
              (#xb200 . accept)
              (0      . ok)
              (5      . again)
              (6      . wrong)
              (7      . checksum)
              (8      . no-reg))
            (- num)))

(define-foreign %fvm-open int "fvm_open" '(* *))
(define (fvm-open dev)
  (let* ((port (make-bytevector (sizeof '*)))
         (r (%fvm-open (string->pointer dev) (bytevector->pointer port)))
         (ref (case (sizeof '*)
                ((4) bytevector-u32-native-ref)
                ((8) bytevector-u64-native-ref)
                (else (error "Wrong size of a pointer ~a" (sizeof '*))))))
    (if (= r 0)
        (make-pointer (ref port 0))
        (error "Failed to open ~a" dev))))

(define-foreign fvm-close int "fvm_close" '(*))

(define (call-with-open-fvm dev proc)
  (let ((serial-port #f))
    (dynamic-wind
      (lambda () (set! serial-port (fvm-open dev)))
      (lambda () (proc serial-port))
      (lambda () (fvm-close serial-port)))))

(define-foreign fvm-get-enroll-fv-count
  int "fvm_get_enroll_fv_count" '(* * *))

(define* (get-enroll-fv-count serial-port #:optional block)
  (let* ((userid-count (make-bytevector (sizeof size_t)))
         (fingerid-count (make-bytevector (sizeof size_t)))
         (get-count (lambda ()
                      (let ((r (fvm-get-enroll-fv-count
                                serial-port
                                (bytevector->pointer userid-count)
                                (bytevector->pointer fingerid-count))))
                        (if (eq? (fvm-error r) 'ok)
                            ;; TODO Return also `fingerid-count'
                            (bytevector-u16-native-ref userid-count 0)
                            (if (eq? (fvm-error r) 'again)
                                'again
                                (error "Failed to get enroll FV count: ~a~%"
                                       r)))))))
    (if (not block)
        (get-count)
        (let lp ((r (get-count)))
          (if (eq? r 'again)
              (lp (get-count))
              r)))))

(define-foreign fvm-enroll-fv int "fvm_enroll_fv" (list '* uint16 uint8))
(define-foreign %fvm-enroll-fv-status int "fvm_enroll_fv_status" '())
(define-foreign fvm-acquired-fv-count int "fvm_acquired_fv_count" '())
(define-foreign fvm-cancel-enroll-fv int "fvm_cancel_enroll_fv" '(*))

(define (fvm-enroll-fv-status)
  (assv-ref '((0 . continue-collecting)
              (1 . registration-success)
              (2 . registration-failed)
              (3 . registration-failed-timeout))
            (%fvm-enroll-fv-status)))

(define enroll-user-id #f)
(define enroll-proc #f)

(define (start-enroll-fv serial-port new-user-id)
  (let ((r (fvm-enroll-fv serial-port new-user-id 0)))
    (if (eq? (fvm-error r) 'again)
        (begin
          (set! enroll-user-id new-user-id)
          (set! enroll-proc (lambda ()
                              (fvm-enroll-fv serial-port new-user-id 0))))
        (error "Failed to start enroll: ~a (~a)~%" (fvm-error r) r))))

(define (call-with-enroll-process acquired completed failed)
  (when (procedure? enroll-proc)
    (let ((r (enroll-proc)))
      (cond
       ((and (eq? (fvm-error r) 'ok)
             (eq? (fvm-enroll-fv-status) 'registration-success))
        (set! enroll-proc #f)
        (completed enroll-user-id))
       ((eq? (fvm-error r) 'again)
        (acquired (fvm-acquired-fv-count)))
       (else
        (set! enroll-proc #f)
        (failed (fvm-error r)))))))

(define (cancel-enroll-fv serial-port)
  (fvm-error (fvm-cancel-enroll-fv serial-port)))

(define-foreign fvm-identify-fv int "fvm_identify_fv" '(*))

(define (call-with-identify-fv-result serial-port identified failed)
  (let ((r (fvm-identify-fv serial-port)))
    (cond
     ((> r 0) (identified r))
     ((< r 0) (failed r)))))
