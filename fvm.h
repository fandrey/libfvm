/* Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
 *
 * This file is part of libfvm.
 *
 * libfvm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * libfvm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libfvm.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#ifndef FVM_H
#define FVM_H

#include <stdio.h>
#include <stdint.h>
#include <libserialport.h>

#define FVM_ERROR(format, ...) fprintf(stderr, format, __VA_ARGS__)

enum
  {
   /* Command processing succeeded */
   FVM_SUCCESS               = -(0x11 << 8),
   /* Command processing failed */
   FVM_ERR_FAIL              = -(0x01 << 8),
   /* There is no registered template in the specified number */
   FVM_ERR_TMPL_EMPTY        = -(0x13 << 8),
   /* Template already exists in the specified number */
   FVM_ERR_TMPL_NOT_EMPTY    = -(0x14 << 8),
   /* There is no registered template */
   FVM_ERR_ALL_TMPL_EMPTY    = -(0x15 << 8),
   /* There is no registrable Template ID */
   FVM_ERR_EMPTY_ID_NOEXIST  = -(0x16 << 8),
   /* There is no corrupted Template */
   FVM_ERR_BROKEN_ID_NOEXIST = -(0x17 << 8),
   /* Finger vein has been registered */
   FVM_ERR_DUPLICATION_ID    = -(0x19 << 8),
   /* The image quality of finger vein is not good */
   FVM_ERR_BAD_QUALITY       = -(0x1B << 8),
   /* The specified template number is incorrect */
   FVM_ERR_INVALID_TMPL_NO   = -(0x21 << 8),
   /* Invalid parameters used */
   FVM_ERR_INVALID_PARAM     = -(0x25 << 8),
   /* No image is captured */
   FVM_ERR_EMPTY_FP_IMAGE    = -(0x26 << 8),
   /* Next image capture timeout */
   FVM_ERR_FP_IMAGE_TIMEOUT  = -(0x2B << 8),
   /* Storage is full */
   FVM_ERR_STORE_FULL        = -(0xB0 << 8),
   /* Operation timeout */
   FVM_ERR_TIMEOUT           = -(0xB1 << 8),
   /* Commands accepted but takes time to process */
   FVM_ERR_ACCEPT            = -(0xB2 << 8),
   /* Response is not received yet. Try again. */
   FVM_ERR_AGAIN             = SP_ERR_SUPP - 1,
   /* Wrong PIC or RCM */
   FVM_ERR_WRONG             = SP_ERR_SUPP - 2,
   /* Checksum does not match */
   FVM_ERR_CHECKSUM          = SP_ERR_SUPP - 3,
   /* No registration */
   FVM_ERR_NOREG             = SP_ERR_SUPP - 4
  };

enum fvm_registration_status
  {
   FVM_CONTINUE_COLLECTING = 0,
   FVM_REGISTRATION_SUCCESS,
   FVM_REGISTRATION_FAILED,
   FVM_REGISTRATION_FAILED_TIMEOUT
  };

void enable_print_packets();
void disable_print_packets();

enum sp_return fvm_open(char *dev, struct sp_port **port);
enum sp_return fvm_close(struct sp_port *port);

enum sp_return fvm_send(struct sp_port *port, uint8_t cmd, uint8_t *data);
enum sp_return fvm_receive(struct sp_port *port, void *data, size_t *data_size);

enum sp_return fvm_get_enroll_fv_count(struct sp_port *port,
                                       size_t *userid_count,
                                       size_t *fvid_count);

int fvm_acquired_fv_count(void);
int fvm_last_acquired_fv_quality(void);
enum fvm_registration_status fvm_enroll_fv_status();

enum sp_return fvm_enroll_fv(struct sp_port *port,
                             uint16_t userid, uint8_t fingerid);

enum sp_return fvm_cancel_enroll_fv(struct sp_port *port);

enum sp_return fvm_identify_fv(struct sp_port *port);

#endif  /* FVM_H */
