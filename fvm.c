/* Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
 *
 * This file is part of libfvm.
 *
 * libfvm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * libfvm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libfvm.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "fvm.h"

#define CHECK_RETCODE(expr)                                             \
  {                                                                     \
    enum sp_return ret = (expr);                                        \
    if (ret != SP_OK)                                                   \
      {                                                                 \
        FVM_ERROR("%s: line: %d: ret %d: %s\n",                         \
              __func__, __LINE__, ret, sp_last_error_message());        \
        return ret;                                                     \
      }                                                                 \
  }

#define XTFV_GET_DEV_INFO        0x01
#define XTFV_UPGRADE_FIRMWARE    0x11
#define XTFV_READ_FV_DATA        0x27
#define XTFV_WRITE_FV_DATA       0x28
#define XTFV_GET_ENROLL_FV_COUNT 0x43
#define XTFV_ENROLL_FV           0x45
#define XTFV_GET_ENROLL_ID       0x47
#define XTFV_CLEAR_FV            0x48
#define XTFV_CLEAR_ALL_FV        0x49
#define XTFV_VERIFY_FV           0x4A
#define XTFV_IDENTIFY_FV         0x4B
#define XTFV_GET_ENROLL_ID_LIST  0x5A
#define XTFV_SEARCH_ID           0x5B
#define XTFV_POWER_DOWN          0xA2

#define COMMAND_PIC       0xAA
#define RESPONSE_PIC      0x55
#define COMMAND_DATA_PIC  0xA5
#define RESPONSE_DATA_PIC 0x5A

#define FVM_COMMAND_PACKET_DATA_SIZE 9

struct __attribute__ ((__packed__)) fvm_command_packet
{
  uint8_t  pic;                 /* Packet Identify Code (0xAA) */
  uint8_t  siddid[2];           /* Source Device ID and Destination
                                   Device ID are reserved for future
                                   use. */
  uint8_t  cmd;                 /* Command Code */
  uint8_t  data[FVM_COMMAND_PACKET_DATA_SIZE]; /* Command Parameter */
  uint8_t  key;                 /* Packet encryption key is reserved
                                   for future */
  uint16_t csm;                 /* Checksum */
};

struct __attribute__ ((__packed__)) fvm_response_packet
{
  uint8_t  pic;                 /* Packet Identify Code (0x55) */
  uint8_t  siddid[2];           /* Source Device ID and Destination
                                   Device ID are reserved for future
                                   use. */
  uint8_t  rcm;                 /* Response Code */
  uint8_t  err;                 /* Result Code */
  uint8_t  data[4];             /* Command Parameter */
  uint32_t uid;                 /* Device UID is reserved
                                   for future */
  uint8_t  key;                 /* Packet encryption key is reserved
                                   for future */
  uint16_t csm;                 /* Checksum */
};

struct __attribute__ ((__packed__)) fvm_command_data_packet_head
{
  uint8_t  pic;                 /* Packet Identify Code (0xA5) */
  uint8_t  siddid[2];           /* Source Device ID and Destination
                                   Device ID are reserved for future
                                   use. */
  uint8_t  cmd;                 /* Command Code */
  uint8_t  data[9];             /* Command Parameter */
};

struct __attribute__ ((__packed__)) fvm_command_data_packet_tail
{
  uint8_t  key;                 /* Packet encryption key is reserved
                                   for future */
  uint16_t csm;                 /* Checksum */
};

struct __attribute__ ((__packed__)) fvm_response_data_packet_head
{
  uint8_t  pic;                 /* Packet Identify Code (0x5A) */
  uint8_t  siddid[2];           /* Source Device ID and Destination
                                   Device ID are reserved for future
                                   use. */
  uint8_t  rcm;                 /* Response Code */
  uint8_t  err;                 /* Result Code */
  uint8_t  data[4];             /* Command Parameter */
};

struct __attribute__ ((__packed__)) fvm_response_data_packet_tail
{
  uint32_t uid;                 /* Device UID is reserved
                                   for future */
  uint8_t  key;                 /* Packet encryption key is reserved
                                   for future */
  uint16_t csm;                 /* Checksum */
};

uint16_t checksum(uint8_t *packet, size_t size)
{
  int i;
  uint16_t sum = 0;

  for (i = 0; i < size - sizeof(uint16_t); i++)
    sum += packet[i];

  return sum;
}

#define hex_asc_lo(x)	hex_asc[((x) & 0x0f)]
#define hex_asc_hi(x)	hex_asc[((x) & 0xf0) >> 4]

static char *hex_byte_pack(char *buf, unsigned char byte)
{
  const static char hex_asc[] = "0123456789abcdef";

  *buf++ = hex_asc_hi(byte);
  *buf++ = hex_asc_lo(byte);
  return buf;
}

static char *hexdump(char *dst, const void *src, size_t count)
{
  const unsigned char *s = src;

  while (count--)
    {
      dst = hex_byte_pack(dst, *s++);
      *dst++ = ' ';
    }

  *(dst - 1) = '\0';
  return dst;
}

static void print_packet_to_stdout(char *prefix, void *packet, size_t size)
{
  static char buf[2048];
  hexdump(buf, packet, size);
  printf("%s%s\n", prefix, buf);
  fflush(stdout);
}

static void dont_print_packet(char *prefix, void *packet, size_t size)
{
}

typedef void (*print_packet_function_t)(char *prefix,
                                        void *packet,
                                        size_t size);

static print_packet_function_t print_packet = dont_print_packet;

void enable_print_packets()
{
  print_packet = print_packet_to_stdout;
}

void disable_print_packets()
{
  print_packet = dont_print_packet;
}

enum sp_return fvm_open(char *dev, struct sp_port **port)
{
  CHECK_RETCODE(sp_get_port_by_name(dev, port));
  CHECK_RETCODE(sp_open(*port, SP_MODE_READ_WRITE));
  CHECK_RETCODE(sp_set_baudrate(*port, 115200));
  CHECK_RETCODE(sp_set_bits(*port, 8));
  CHECK_RETCODE(sp_set_parity(*port, SP_PARITY_NONE));
  CHECK_RETCODE(sp_set_stopbits(*port, 1));
  CHECK_RETCODE(sp_set_flowcontrol(*port, SP_FLOWCONTROL_NONE));

  return SP_OK;
}

enum sp_return fvm_close(struct sp_port *port)
{
  CHECK_RETCODE(sp_close(port));
  sp_free_port(port);

  return SP_OK;
}

enum sp_return fvm_send(struct sp_port *port, uint8_t cmd, uint8_t *data)
{
  enum sp_return ret;
  struct fvm_command_packet packet;
  size_t size = sizeof(struct fvm_command_packet);

  memset(&packet, 0, size);
  packet.pic = COMMAND_PIC;
  packet.cmd = cmd;
  if (data)
    memcpy(packet.data, data, sizeof(packet.data));
  packet.csm = checksum((uint8_t*)&packet, size);

  print_packet("Send packet:    ", &packet, size);

  if ((ret = sp_nonblocking_write(port, &packet, size)) < 0)
    return ret;

  return SP_OK;
}

enum
  {
   PACKET_NOT_FOUND = -1,
   PACKET_FOUND,
   PACKET_INCOMPLETE
  };

int find_packet(uint8_t *buf, size_t buf_size, size_t *start, size_t *end)
{
  size_t i;
  for (i = 0; i < buf_size - 2; ++i)
    if (!buf[i+1] && !buf[i+2])
      {
        if (buf[i] == RESPONSE_PIC)
          {
            *start = i;
            *end   = *start + sizeof(struct fvm_response_packet);
            return buf_size < (i + sizeof(struct fvm_response_packet)) ?
              PACKET_INCOMPLETE : PACKET_FOUND;
          }
        else if (buf[i] == RESPONSE_DATA_PIC)
          {
            size_t data_size, sizeof_head = sizeof(struct fvm_response_data_packet_head);
            struct fvm_response_data_packet_head *head =
              (struct fvm_response_data_packet_head *)(buf + i);

            *start = i;
            if (buf_size < (i + sizeof_head))
              return PACKET_INCOMPLETE;

            data_size = *(uint16_t *)head->data;

            if (buf_size <
                (i + sizeof_head + data_size +
                 (sizeof(struct fvm_response_data_packet_tail))))
              return PACKET_INCOMPLETE;

            *end = *start + i + sizeof_head + data_size +
              sizeof(struct fvm_response_data_packet_tail);
            return PACKET_FOUND;
          }
      }

  return PACKET_NOT_FOUND;
}

enum sp_return fvm_receive(struct sp_port *port, void *data, size_t *data_size)
{
  static uint8_t buf[1024];
  static size_t have = 0;
  size_t packet_start = 0, packet_end = 0;
  ssize_t r;
  int found;

  if ((r = sp_nonblocking_read(port, buf + have, sizeof(buf) - have)) <= 0)
    return r;

  have += r;

  found = find_packet(buf, have, &packet_start, &packet_end);
  if (found == PACKET_FOUND)
    {
      *data_size = packet_end - packet_start;
      memcpy(data, buf + packet_start, *data_size);

      have -= packet_end;
      if (have)
        memmove(buf, buf + packet_end, have);

      print_packet("Receive packet: ", data, *data_size);

      return 1;
    }

  if (found == PACKET_INCOMPLETE)
    {
      have -= packet_start;
      if (packet_start)
        memmove(buf, buf + packet_start, have);

      return 0;
    }

  have = 0;
  return 0;
}

enum sp_return fvm_send_and_receive(struct sp_port *port,
                                    uint8_t cmd, uint8_t *cmd_data,
                                    void *rcv_data, size_t *rcv_data_size)
{
  static int command_sent = 0;
  ssize_t r;

  if (!command_sent && (r = fvm_send(port, cmd, cmd_data)) < 0)
    return r;

  command_sent = 1;

  if (!(r = fvm_receive(port, rcv_data, rcv_data_size)))
    return FVM_ERR_AGAIN;

  command_sent = 0;
  return r < 0 ? r : SP_OK;
}

static inline int deverr_to_enumerr(uint8_t err)
{
  return -(err << 8);
}

enum sp_return check_response(struct fvm_response_packet *response, uint8_t rcm)
{
  if (response->pic != RESPONSE_PIC || response->rcm != rcm)
    return FVM_ERR_WRONG;

  if (response->csm != checksum((uint8_t*)response, sizeof(struct fvm_response_packet)))
    return FVM_ERR_CHECKSUM;

  return SP_OK;
}

enum sp_return fvm_get_enroll_fv_count(struct sp_port *port,
                                       size_t *userid_count, size_t *fvid_count)
{
  static uint8_t buf[1024];
  size_t data_size;
  struct fvm_response_packet *response = (struct fvm_response_packet *)buf;
  ssize_t r;

  if ((r = fvm_send_and_receive(port,
                                XTFV_GET_ENROLL_FV_COUNT, NULL,
                                buf, &data_size)) < 0)
    return r;

  if ((r = check_response(response, XTFV_GET_ENROLL_FV_COUNT)) != SP_OK)
    return r;

  if (response->err &&
      deverr_to_enumerr(response->err) != FVM_SUCCESS)
    return deverr_to_enumerr(response->err);

  *userid_count = *(uint16_t *)response->data;
  *fvid_count = *(uint16_t *)(response->data + 2);

  return SP_OK;
}

/* TODO Describe the registration algorithm */

static enum sp_return init_enroll_fv(struct sp_port *port,
                                     uint16_t userid,
                                     uint8_t fingerid);

static enum sp_return wait_for_fv_acquisition(struct sp_port *port,
                                              uint16_t userid,
                                              uint8_t fingerid);

typedef enum sp_return (*enroll_function_t)(struct sp_port *port,
                                            uint16_t userid, uint8_t fingerid);

static enroll_function_t enroll_function = init_enroll_fv;
static uint8_t enroll_command_data[FVM_COMMAND_PACKET_DATA_SIZE];
static int acquired_fvs = 0, acquired_fv_quality = 0, registration_status;
const int acquired_fvs_total = 3;
static enum sp_return last_error;

int fvm_acquired_fv_count(void)
{
  return acquired_fvs;
}

int fvm_last_acquired_fv_quality(void)
{
  return acquired_fv_quality;
}

enum fvm_registration_status fvm_enroll_fv_status()
{
  return registration_status;
}

static enum sp_return cancel_and_reset_enroll(struct sp_port *port,
                                              uint16_t userid, uint8_t fingerid)
{
  ssize_t r;

  if ((r = fvm_cancel_enroll_fv(port)) < 0)
    return r;

  return last_error;
}

static enum sp_return request_acquire_fv(struct sp_port *port,
                                         uint16_t userid, uint8_t fingerid)
{
  ssize_t r;
  static uint8_t response_buf[1024];
  size_t response_size;
  struct fvm_response_packet *response =
    (struct fvm_response_packet *)response_buf;

  if ((r = fvm_send_and_receive(port,
                                XTFV_ENROLL_FV, enroll_command_data,
                                response_buf, &response_size)) < 0)
    {
      if (r == FVM_ERR_AGAIN)
        return r;

      enroll_function = cancel_and_reset_enroll;
      last_error = r;
      return cancel_and_reset_enroll(port, userid, fingerid);
    }

  if ((r = check_response(response, XTFV_ENROLL_FV)) != SP_OK)
    {
      enroll_function = cancel_and_reset_enroll;
      last_error = r;
      return cancel_and_reset_enroll(port, userid, fingerid);
    }

  if (response->err)
    {
      int e = deverr_to_enumerr(response->err);

      if (e == FVM_ERR_ACCEPT)
        {
          enroll_function = wait_for_fv_acquisition;
          return FVM_ERR_AGAIN;
        }

      enroll_function = cancel_and_reset_enroll;
      last_error = e;
      return cancel_and_reset_enroll(port, userid, fingerid);
    }

  /* TODO Return an error and cancel registration.  The device should
     return FVM_ERR_ACCEPT. */
  return SP_OK;
}

static enum sp_return wait_for_fv_acquisition(struct sp_port *port,
                                              uint16_t userid, uint8_t fingerid)
{
  ssize_t r;
  static uint8_t response_buf[1024];
  size_t response_size;
  struct fvm_response_packet *response =
    (struct fvm_response_packet *)response_buf;

  if ((r = fvm_receive(port, response_buf, &response_size)) < 0)
    {
      enroll_function = cancel_and_reset_enroll;
      last_error = r;
      return cancel_and_reset_enroll(port, userid, fingerid);
    }

  if (!r)
    return FVM_ERR_AGAIN;

  if ((r = check_response(response, XTFV_ENROLL_FV)) != SP_OK)
    {
      enroll_function = cancel_and_reset_enroll;
      last_error = r;
      return cancel_and_reset_enroll(port, userid, fingerid);
    }

  if (response->err)
    {
      registration_status = response->data[2];
      enroll_function = cancel_and_reset_enroll;
      last_error = deverr_to_enumerr(response->err);
      return cancel_and_reset_enroll(port, userid, fingerid);
    }

  acquired_fvs = response->data[0];
  acquired_fv_quality = response->data[1];
  registration_status = response->data[2];

  if (registration_status == FVM_REGISTRATION_SUCCESS)
    {
      enroll_function = init_enroll_fv;
      r = SP_OK;
    }
  else
    {
      enroll_command_data[3]++;   /* Next acquisition step */
      enroll_function = request_acquire_fv;
      r = FVM_ERR_AGAIN;
    }

  return r;
}

static enum sp_return init_enroll_fv(struct sp_port *port,
                                     uint16_t userid, uint8_t fingerid)
{
  ssize_t r;
  static uint8_t response_buf[1024];
  size_t response_size;
  struct fvm_response_packet *response =
    (struct fvm_response_packet *)response_buf;

  memset(&enroll_command_data, 0, sizeof(enroll_command_data));
  *(uint16_t *)enroll_command_data = userid;
  enroll_command_data[sizeof(userid)] = fingerid;
  registration_status = FVM_CONTINUE_COLLECTING;
  acquired_fvs = 0;

  if ((r = fvm_send_and_receive(port,
                                XTFV_ENROLL_FV, enroll_command_data,
                                response_buf, &response_size)) < 0)
    return r;

  if ((r = check_response(response, XTFV_ENROLL_FV)) != SP_OK)
    return r;

  if (response->err)
    return deverr_to_enumerr(response->err);

  enroll_command_data[3]++;   /* The acquisition step 1 */
  enroll_function = request_acquire_fv;

  return FVM_ERR_AGAIN;
}

enum sp_return fvm_enroll_fv(struct sp_port *port,
                             uint16_t userid, uint8_t fingerid)
{
  return enroll_function(port, userid, fingerid);
}

enum sp_return fvm_cancel_enroll_fv(struct sp_port *port)
{
  ssize_t r;
  static uint8_t response_buf[1024];
  size_t response_size;
  struct fvm_response_packet *response =
    (struct fvm_response_packet *)response_buf;

  if (enroll_function == init_enroll_fv)
    return FVM_ERR_NOREG;

  enroll_command_data[5] = 1;
  if ((r = fvm_send_and_receive(port,
                                XTFV_ENROLL_FV, enroll_command_data,
                                response_buf, &response_size)) < 0)
    return r;

  if ((r = check_response(response, XTFV_ENROLL_FV)) != SP_OK)
    return r;

  registration_status = response->data[2];
  enroll_function = init_enroll_fv;

  if (response->err && deverr_to_enumerr(response->err) != FVM_ERR_FAIL)
    return deverr_to_enumerr(response->err);

  return SP_OK;
}

enum sp_return fvm_identify_fv(struct sp_port *port)
{
  int r;
  uint8_t command_data[FVM_COMMAND_PACKET_DATA_SIZE];
  struct fvm_response_packet response;
  size_t response_size = sizeof(response);

  struct fvm_identify_data
  {
    uint16_t user_id;
    uint8_t finger_id;
  };
  struct fvm_identify_data
    *command_idata = (struct fvm_identify_data *)command_data,
    *response_idata = (struct fvm_identify_data *)&response.data;

  r = fvm_receive(port, &response, &response_size);

  if (!r || r < 0)
    return r;

  if ((r = check_response(&response, XTFV_IDENTIFY_FV)) != SP_OK)
    return r;

  memset(&command_data, 0, sizeof(command_data));

  if (!response.err)
    *command_idata = *response_idata;

  /* Send the XTFV_IDENTIFY_FV command as a host response */
  if ((r = fvm_send(port, XTFV_IDENTIFY_FV, command_data)) != SP_OK)
    return r;

  /* TODO Also return FingerID */
  return response.err ?
    deverr_to_enumerr(response.err) : response_idata->user_id;
}
